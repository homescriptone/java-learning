package advanced_object;

abstract class Animal {
	protected double poids;
	protected String couleur;
	protected static void manger() {
		System.out.println(" Je mange de la viande ");
	}
	public static void boire() {
		System.out.println(" Je bois de la biere ");
	}
	abstract void deplacement();
	abstract void crier();
	
	public String toString(){
		    String str = "Je suis un objet de la " + this.getClass() + ", je suis " + this.couleur + ", je pèse " + this.poids;
		    return str;
	}  
}
